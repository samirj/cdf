#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <cdf.h>
#include "../../db_entity/samples/customer.h"

#define PORT 19876
HttpServer * server = NULL;


typedef struct {
    inherits(HttpRequestHandler);
    void (*handle)(void *, HttpRequest *, HttpResponse *);
} WwwRequestHandler;

void WwwRequestHandler_handle(void * _this, HttpRequest * request, HttpResponse * response) {
    Customer * customer = new(Customer, REFCTMP(new(String, "John")), REFCTMP(new(String, "Doe")), REFCTMP(new(Integer, 23)));
    String * s = call((Entity*)customer, to_json, NULL);
    call(s, append_char, '\n');
    call(response, append_content, s);

    call(response, add_header, REFCTMP(new(HttpHeader, REFCTMP(new(String, "Content-Type")), REFCTMP(new(String, "application/json")))));

    delete(s);
    delete(customer);
}

void WwwRequestHandler_delete(void * _this) {
    super_delete(HttpRequestHandler, _this);
}

WwwRequestHandler * WwwRequestHandler_new(WwwRequestHandler * this) {
    super(HttpRequestHandler, WwwRequestHandler);
    override(HttpRequestHandler, handle, WwwRequestHandler_handle);
    return this;
}


void signal_handler(int signal) {
    switch(signal) {
    case SIGINT:
        printf("\nStopping the server...\n");
        if(server != NULL) {
            call(server, stop);
        }
        break;
    }
}

int main(void) {
    signal(SIGINT, signal_handler);
    server = new(HttpServer, PORT, (HttpRequestHandler *) new(WwwRequestHandler));
    printf("Starting the server on port %d\n", PORT);
    call(server, start);
    if(call(server, is_running)) {
        printf("Server started\n");
    }
    while(call(server, is_running)) {
        sleep(1);
    }
    delete(server);
    printf("Bye bye\n");
    return 0;
}


