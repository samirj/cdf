#include "parserhandlers.h"


void _obeh_value(ObjectPtr _this, JsonObject * jo) {
    make_this(JsonObjectBuilderEventsHandler, _this);
    call((List *) this->object->value, add, jo);
}

JsonObjectBuilderEventsHandler * JsonObjectBuilderEventsHandler_new(JsonObjectBuilderEventsHandler * this) {
    super(JsonEventsHandler, JsonObjectBuilderEventsHandler);
    override(JsonEventsHandler, value, _obeh_value);
    this->object = new(JsonObject, REFCTMP(new(List)));
    return this;
}

void JsonObjectBuilderEventsHandler_delete(ObjectPtr _this) {
    make_this(JsonObjectBuilderEventsHandler, _this);
    REFCDEC(this->object);
}


