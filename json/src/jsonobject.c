#include "jsonobject.h"
#include <ooc.h>

static const char * ILLEGEAL_NAME_CHARACTERS = " \"'[]{}`~!@#$%^&*()+=,.<>/?\\|";
static bool checkName(String * name) {
    if(name == NULL) {
        return true;
    }

    return !call(name, contains_any_char, ILLEGEAL_NAME_CHARACTERS);
}

String * JsonObject_to_string(ObjectPtr _this) {
    make_this(JsonObject, _this);
    String * s = new(String);
    if(this->name != NULL) {
        call(s, append_char, '"');
        call(s, append, this->name);
        call(s, append_cstring, "\": ");
    }
    if(this->value == NULL) {
        call(s, append_cstring, "null");
    } else {
        if(type_equal(this->value, "String")) {
            call(s, append_char, '"');
            call(s, append_cstring, call((String *) this->value, to_cstring));
            call(s, append_char, '"');
        } else if(type_equal(this->value, "Integer")
                || type_equal(this->value, "Long")
                || type_equal(this->value, "Boolean")
                || type_equal(this->value, "Double")) {
            String * buf = call(this->value, to_string);
            call(s, append, buf);
            delete(buf);
        } else if(type_equal(this->value, "Array")) {
            Array * array = (Array *) this->value;
            call(s, append_char, '[');
            for(int i = 0; i < array->length; ++i) {
                Object * o = call(array, get, i);
                String * buf = call(o, to_string);
                REFCDEC(o);
                call(s, append, buf);
                delete(buf);
                if(i < array->length - 1) {
                    call(s, append_cstring, ", ");
                }
            }
            call(s, append_char, ']');
        } else if(type_equal(this->value, "List")){
            //Object should contain list of JsonObjects
            List * list = (List *) this->value;
            call(s, append_char, '{');
            for(int i = 0; i < list->length; ++i) {
                Object * o = call(list, get, i);
                String * buf = call(o, to_string);
                REFCDEC(o);
                call(s, append, buf);
                delete(buf);
                if(i < list->length - 1) {
                    call(s, append_cstring, ", ");
                }
            }
            call(s, append_char, '}');
        }
    }

    return s;
}

JsonObject * JsonObject_new2(JsonObject * this, String * name, ObjectPtr value) {
    if(!checkName(name)) {
        return NULL;
    }
    super(Object, JsonObject);
    override(Object, to_string, JsonObject_to_string);
    REFCINC(name);
    REFCINC(value);
    this->name = name;
    this->value = value;
    return this;
}

JsonObject * JsonObject_new1(JsonObject * this, ObjectPtr value) {
    this(JsonObject, NULL, value);
    return this;
}

void JsonObject_delete(ObjectPtr _this) {
    make_this(JsonObject, _this);
    REFCDEC(this->name);
    REFCDEC(this->value);
    super_delete(Object, this);
}

