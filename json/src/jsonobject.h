#ifndef JSONOBJECT_H
#define JSONOBJECT_H

#include <ooc_string.h>
#include <stdbool.h>


typedef struct {
    inherits(Object);
    String * name;
    Object * value;
    String * (*to_string)(ObjectPtr);
} JsonObject;

JsonObject * JsonObject_new1(JsonObject *, ObjectPtr);
JsonObject * JsonObject_new2(JsonObject *, String *, ObjectPtr);
void JsonObject_delete(ObjectPtr);


#endif

