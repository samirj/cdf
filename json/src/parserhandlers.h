#ifndef PARSER_HANDLERS_H
#define PARSER_HANDLERS_H

#include "eventparser.h"

typedef struct  {
    inherits(JsonEventsHandler);
    void (*json_begin)(ObjectPtr);
    void (*json_end)(ObjectPtr);
    void (*object_begin)(ObjectPtr, char *);
    void (*object_end)(ObjectPtr);
    void (*array_begin)(ObjectPtr, char *);
    void (*array_end)(ObjectPtr);
    void (*value)(ObjectPtr, JsonObject *);

    JsonObject * object;
} JsonObjectBuilderEventsHandler;

JsonObjectBuilderEventsHandler * JsonObjectBuilderEventsHandler_new(JsonObjectBuilderEventsHandler *);
void JsonObjectBuilderEventsHandler_delete(ObjectPtr);


#endif

