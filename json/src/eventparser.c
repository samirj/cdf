#include "eventparser.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


void JsonEventsHandler_delete(ObjectPtr _this) {
    //empty destructor
    super_delete(Object, _this);
}

JsonEventsHandler * JsonEventsHandler_new(JsonEventsHandler * this) {
    super(Object, JsonEventsHandler);
    this->json_begin = NULL;
    this->json_end = NULL;
    this->value = NULL;
    this->object_begin = NULL;
    this->object_end = NULL;
    this->array_begin = NULL;
    this->array_end = NULL;
    return this;
}

typedef enum {
    IDLE,
    ROOT,
    NAME,
    NAME_DONE,
    READY_FOR_VALUE,
    VALUE

} _State;

typedef enum {
    BOOLEAN,
    DOUBLE,
    INTEGER,
    LONG,
    STRING,
    ARRAY,
    OBJECT
} _Type;


int _parse(ObjectPtr _this, InputStream * json_stream, bool ignore_trailing_characters);

int _JsonEventsParser_parse(ObjectPtr _this, InputStream * json_stream) {
    make_this(JsonEventsParser, _this);
    if(this->_handler == NULL) {
        return CJSON_PARSE_NO_HANDLER;
    }

    if(json_stream == NULL) {
        return CJSON_PARSE_NO_INPUT;
    }

    return _parse(this, json_stream, false);
}


JsonEventsParser * JsonEventsParser_new1(JsonEventsParser * this, JsonEventsHandler * handler) {
    super(Object, JsonEventsParser);
    REFCINC(handler);
    this->_handler = handler;
    this->parse = _JsonEventsParser_parse;
    this->_buffer = new(String);
    this->_name = new(String);
    this->_value = new(String);
    this->_state = IDLE;
    return this;
}


void JsonEventsParser_delete(ObjectPtr _this) {
    make_this(JsonEventsParser, _this);
    REFCDEC(this->_name);
    REFCDEC(this->_buffer);
    REFCDEC(this->_value);
    REFCDEC(this->_handler);
    super_delete(Object, _this);
}

int _processIdle(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);
    if(isspace(c)) {
        return CJSON_PARSE_SUCCESS;
    }
    if(c == '{') {
        this->_state = ROOT;
        if(this->_handler->json_begin != NULL) {
            call(this->_handler, json_begin);
        }
        return CJSON_PARSE_SUCCESS;
    }
    return CJSON_PARSE_INVALID_JSON;
}


int _processRoot(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);
    if(isspace(c)) {
        return CJSON_PARSE_SUCCESS;
    }

    if(c == '}') {
        this->_state = IDLE;
        if(this->_handler->json_end != NULL) {
            call(this->_handler, json_end);
        }
    } else if(c == '"') {
        this->_state = NAME;
    } else {
        return CJSON_PARSE_INVALID_JSON;
    }

    return CJSON_PARSE_SUCCESS;
}

int _processName(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);

    if(c=='"') {
        this->_state = NAME_DONE;
        call(this->_name, set_text, call(this->_buffer, to_cstring));
        call(this->_buffer, clear);
    } else {
        call(this->_buffer, append_char, c);
    }
    return CJSON_PARSE_SUCCESS;
}

int _processNameDone(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);
    if(isspace(c)) {
        return CJSON_PARSE_SUCCESS;
    }

    if(c == ':') {
        this->_state = READY_FOR_VALUE;
        return CJSON_PARSE_SUCCESS;
    }

    return CJSON_PARSE_INVALID_JSON;
}

int _processReadyForValue(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);
    if(isspace(c)) {
        return CJSON_PARSE_SUCCESS;
    }

    call(this->_buffer, append_char, c);

    this->_state = VALUE;
    return CJSON_PARSE_SUCCESS;
}


_Type _guessType(String * s) {
    const char * value = call(s, to_cstring);
    if(strcmp(value, "true") == 0 || strcmp(value, "false") == 0) {
        return BOOLEAN;
    }

    if(value[0] == '"' && value[strlen(value) - 1] == '"') {
        return STRING;
    }

    if(strchr(value, '.') != NULL) {
        return DOUBLE;
    }

    return LONG;
}

Object * _getValue(String * s, _Type type) {
    Object * p = NULL;
    switch(type) {
    case INTEGER: {
       Integer * i = new(Integer);
       call(i, from_string, s);
       p = (Object *)i;
       break;
    }
    case DOUBLE: {
        Double * d = new(Double);
        call(d, from_string, s);
        p = (Object *)d;
        break;
    }
    case STRING: {
        p = (Object *)  call(s, substring, 1, s->length - 1);
        break;
    }
    case BOOLEAN: {
        Boolean * b = new(Boolean);
        call(b, from_string, s);
        p = (Object *)b;
        break;
    }
    case LONG: {
        Long * i = new(Long);
        call(i, from_string, s);
        p = (Object *)i;
        break;
    }
    default:
       break;
    }
    return p;
}

int _processValue(ObjectPtr _this, char c) {
    make_this(JsonEventsParser, _this);
    if(c == ',' || c == '}') {
        call(this->_buffer, trim);
        _Type type = _guessType(this->_buffer);
        Object * value = _getValue(this->_buffer, type);
        if(type < 0) {
            REFCDEC(value);
            return CJSON_PARSE_INVALID_VALUE;
        }
        if(this->_handler->value != NULL) {
            JsonObject * jo = new(JsonObject, REFCTMP(call(this->_name, copy)), value);
            call(this->_handler, value, jo);
            REFCDEC(jo);
        }
        REFCDEC(value);

        this->_state = ROOT;
        call(this->_name, clear);
        call(this->_buffer, clear);
        if(c == '}') {
            _processRoot(this, c);
        }
        return CJSON_PARSE_SUCCESS;
    }

    call(this->_buffer, append_char, c);

    return CJSON_PARSE_SUCCESS;
}

int _parse(ObjectPtr _this, InputStream * json_stream, bool ignore_trailing_characters) {
    make_this(JsonEventsParser, _this);
    while(true) {
        int i = call(json_stream, read);
        if(i < 0) {
            break;
        }
        char c = (char) i;

        int ret = CJSON_PARSE_SUCCESS;

        switch(this->_state) {
        case IDLE:
            ret = _processIdle(this, c);
            break;
        case ROOT:
            ret = _processRoot(this, c);
            break;
        case NAME:
            ret = _processName(this, c);
            break;
        case NAME_DONE:
            ret = _processNameDone(this, c);
            break;
        case READY_FOR_VALUE:
            ret = _processReadyForValue(this, c);
            break;
        case VALUE:
            ret = _processValue(this, c);
            break;
        }
        if(ret != CJSON_PARSE_SUCCESS) {
            return ret;
        }

        //end of parsing
        if(this->_state == IDLE) {
            if(ignore_trailing_characters) {
                return CJSON_PARSE_SUCCESS;
            } else {
                _parse(this, json_stream, true);
            }
        }
    }

    if(this->_state != IDLE) {
        return CJSON_PARSE_INVALID_JSON;
    }

    return CJSON_PARSE_SUCCESS;
}


