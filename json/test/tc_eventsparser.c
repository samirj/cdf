#include "framework.h"

#include "eventparser.h"
#include "parserhandlers.h"

typedef struct {
    inherits(JsonEventsHandler);
    void (*json_begin)(ObjectPtr);
    void (*json_end)(ObjectPtr);
    void (*object_begin)(ObjectPtr, char *);
    void (*object_end)(ObjectPtr);
    void (*array_begin)(ObjectPtr, char *);
    void (*array_end)(ObjectPtr);
    void (*value)(ObjectPtr, JsonObject *);
    int json_begin_count;
    int json_end_count;
    int value_count;
    int value_string_count;
    int value_integer_count;
    int value_real_count;
    int value_bool_count;
    int value_null_count;
} JsonCountingEventsHandler;

JsonCountingEventsHandler * JsonCountingEventsHandler_new(JsonCountingEventsHandler *);
void JsonCountingEventsHandler_delete(ObjectPtr);


void testcase(TEST_CASE_ARGUMENTS) {
    JsonCountingEventsHandler * handler = REFCTMP(new(JsonCountingEventsHandler));
    JsonEventsParser * parser = new(JsonEventsParser, (JsonEventsHandler *) handler);
    String * json = new(String, "{ \"name\": \"John Doe\", \"age\": 45, \"is_active\": false, \"amount\": 123.43 }");
    InputStream * json_stream = new(StringInputStream, json);
    int ret = call(parser, parse, json_stream);
    REFCDEC(json_stream);
    REFCDEC(json);
    ASSERT_EQUAL(ret, CJSON_PARSE_SUCCESS);
    ASSERT_EQUAL(handler->json_begin_count, 1);
    ASSERT_EQUAL(handler->json_end_count, 1);
    ASSERT_EQUAL(handler->value_count, 4);
    ASSERT_EQUAL(handler->value_integer_count, 1);
    ASSERT_EQUAL(handler->value_bool_count, 1);
    ASSERT_EQUAL(handler->value_string_count, 1);
    ASSERT_EQUAL(handler->value_real_count, 1);

    REFCDEC(parser);
}

void nohandler(TEST_CASE_ARGUMENTS) {
    JsonEventsParser * parser = new(JsonEventsParser, NULL);
    String * json = new(String, "{ }");
    InputStream * json_stream = new(StringInputStream, json);
    int ret = call(parser, parse, json_stream);
    REFCDEC(json_stream);
    REFCDEC(json);
    ASSERT_EQUAL(ret, CJSON_PARSE_NO_HANDLER);
    REFCDEC(parser);
}

void null_input(TEST_CASE_ARGUMENTS) {
    JsonCountingEventsHandler * handler = REFCTMP(new(JsonCountingEventsHandler));
    JsonEventsParser * parser = new(JsonEventsParser,(JsonEventsHandler *) handler);
    int ret = call(parser, parse, NULL);
    ASSERT_EQUAL(ret, CJSON_PARSE_NO_INPUT);
    REFCDEC(parser);
}


void invalid_json_tc1(TEST_CASE_ARGUMENTS) {
    JsonCountingEventsHandler * handler = REFCTMP(new(JsonCountingEventsHandler));
    JsonEventsParser * parser = new(JsonEventsParser, (JsonEventsHandler *) handler);
    String * json = new(String, "123{ }");
    InputStream * json_stream = new(StringInputStream, json);
    int ret = call(parser, parse, json_stream);
    REFCDEC(json_stream);
    REFCDEC(json);
    ASSERT_EQUAL(ret, CJSON_PARSE_INVALID_JSON);
    REFCDEC(parser);
}

void invalid_json_tc2(TEST_CASE_ARGUMENTS) {
    JsonCountingEventsHandler * handler = REFCTMP(new(JsonCountingEventsHandler));
    JsonEventsParser * parser = new(JsonEventsParser, (JsonEventsHandler *) handler);
    String * json = new(String, "{ } 32");
    InputStream * json_stream = new(StringInputStream, json);
    int ret = call(parser, parse, json_stream);
    REFCDEC(json_stream);
    REFCDEC(json);
    ASSERT_EQUAL(ret, CJSON_PARSE_INVALID_JSON);
    REFCDEC(parser);
}

void build_object(TEST_CASE_ARGUMENTS) {
    JsonObjectBuilderEventsHandler * handler = REFCTMP(new(JsonObjectBuilderEventsHandler));
    JsonEventsParser * parser = new(JsonEventsParser, (JsonEventsHandler *) handler);

    const char * expected_json = "{\"name\": \"John Doe\", \"age\": 45, \"is_active\": false, \"amount\": 123.4300}";

    String * json = new(String, "{ \"name\": \"John Doe\", \"age\": 45, \"is_active\": false, \"amount\": 123.43 }");
    InputStream * json_stream = new(StringInputStream, json);
    int ret = call(parser, parse, json_stream);
    REFCDEC(json_stream);
    REFCDEC(json);
    ASSERT_EQUAL(ret, CJSON_PARSE_SUCCESS);

    String * object_string = call(handler->object, to_string);
    ASSERT_STRINGS_EQUAL(call(object_string, to_cstring), expected_json);

    REFCDEC(object_string);
    REFCDEC(parser);
}


TEST_CASES_BEGIN
    TEST_CASE(testcase);
    TEST_CASE(nohandler);
    TEST_CASE(null_input);
    TEST_CASE(invalid_json_tc1);
    TEST_CASE(invalid_json_tc2);
    TEST_CASE(build_object);
TEST_CASES_END


void _ceh_json_begin(ObjectPtr _this) {
    make_this(JsonCountingEventsHandler, _this);
    this->json_begin_count++;
    printf("json_begin\n");
}


void _ceh_json_end(ObjectPtr _this) {
    make_this(JsonCountingEventsHandler, _this);
    this->json_end_count++;
    printf("json_end\n");
}

void _ceh_value(ObjectPtr _this, JsonObject * jo) {
    make_this(JsonCountingEventsHandler, _this);
    const char * name_cstring = jo->name == NULL ? "null" : call(jo->name, to_cstring);
    String * value_string = jo->value == NULL ? NULL : call(jo->value, to_string);
    const char * value_cstring = value_string == NULL ? "null" : call(value_string, to_cstring);
    this->value_count++;
    if(jo->value == NULL) {
        this->value_null_count++;
        printf("NULL (%s=%s)\n", name_cstring, value_cstring);
    } else if(type_equal(jo->value, "Integer") || type_equal(jo->value, "Long")) {
        this->value_integer_count++;
        printf("INTEGER (%s=%s)\n", name_cstring, value_cstring);
    } else if(type_equal(jo->value, "Double")) {
        this->value_real_count++;
        printf("REAL (%s=%s)\n", name_cstring, value_cstring);
    } else if(type_equal(jo->value, "Boolean")) {
        this->value_bool_count++;
        printf("BOOLEAN (%s=%s)\n", name_cstring, value_cstring);
    } else if(type_equal(jo->value, "String")) {
        printf("STRING (%s=%s)\n", name_cstring, value_cstring);
        this->value_string_count++;
    } else {
        //TODO implement remaining value types
    }
    REFCDEC(value_string);
}

void JsonCountingEventsHandler_delete(ObjectPtr _this) {
    //empty destructor
    super_delete(JsonEventsHandler, _this);
}

JsonCountingEventsHandler * JsonCountingEventsHandler_new(JsonCountingEventsHandler * this) {
    super(JsonEventsHandler, JsonCountingEventsHandler);
    override(JsonEventsHandler, json_begin, _ceh_json_begin);
    override(JsonEventsHandler, json_end, _ceh_json_end);
    override(JsonEventsHandler, value, _ceh_value);
    this->json_begin_count = 0;
    this->json_end_count = 0;
    this->value_count = 0;
    this->value_string_count = 0;
    this->value_integer_count = 0;
    this->value_real_count = 0;
    this->value_bool_count = 0;
    this->value_null_count = 0;
    return this;
}


