#include "framework.h"
#include "jsonobject.h"

#include <ooc.h>

void integer_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_value")), REFCTMP(new(Integer, 321)));
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "Integer"));
    Integer * value = (Integer *) json->value;
    ASSERT_EQUAL(value->value, 321);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_value");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_value\": 321");
    REFCDEC(json_string);
    REFCDEC(json);
}

void long_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_value")), REFCTMP(new(Long, 321)));
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "Long"));
    Integer * value = (Integer *) json->value;
    ASSERT_EQUAL(value->value, 321);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_value");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_value\": 321");
    REFCDEC(json_string);
    REFCDEC(json);
}

void null_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_null_value")), NULL);
    ASSERT_NOT_NULL(json);
    ASSERT_NULL(json->value);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_null_value");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_null_value\": null");
    REFCDEC(json_string);
    REFCDEC(json);
}


void double_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_double")), REFCTMP(new(Double, 123.45)));
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "Double"));
    ASSERT_NOT_NULL(json->value);
    Double * value = (Double *) json->value;
    ASSERT_ALMOST_EQUAL(value->value, 123.45);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_double");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_double\": 123.4500");
    REFCDEC(json_string);
    REFCDEC(json);
}

void string_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_string")), REFCTMP(new(String, " this is my string value 123 _")));
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "String"));
    ASSERT_NOT_NULL(json->value);
    String * value = (String *) json->value;
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_string");
    ASSERT_STRINGS_EQUAL(call(value, to_cstring), " this is my string value 123 _");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_string\": \" this is my string value 123 _\"");
    REFCDEC(json_string);
    REFCDEC(json);
}

void boolean_to_json(TEST_CASE_ARGUMENTS) {
    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_bool")), REFCTMP(new(Boolean, true)));
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "Boolean"));
    ASSERT_NOT_NULL(json->value);
    Boolean * value = (Boolean *) json->value;
    ASSERT_TRUE(value->value);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_bool");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_bool\": true");
    REFCDEC(json_string);
    REFCDEC(json);
}

void invalid_name(TEST_CASE_ARGUMENTS) {
    String * name = new(String, "@#!@");
    JsonObject * jo = new(JsonObject, name, NULL);
    ASSERT_NULL(jo);
    REFCDEC(name);
}

void array_to_json(TEST_CASE_ARGUMENTS) {
    Array * a = REFCTMP(new(Array, 3));
    call(a, set, 0, REFCTMP(new(Integer, 5)));
    call(a, set, 1, REFCTMP(new(Integer, 10)));
    call(a, set, 2, REFCTMP(new(JsonObject, REFCTMP(new(Integer, 15)))));

    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_array")), a);
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "Array"));
    ASSERT_NOT_NULL(json->value);
    Array * value = (Array *) json->value;
    ASSERT_EQUAL(value->length, 3);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_array");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_array\": [5, 10, 15]");
    REFCDEC(json_string);
    REFCDEC(json);
}

void object_to_json(TEST_CASE_ARGUMENTS) {
    List * a = REFCTMP(new(List));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "name")), REFCTMP(new(String, "John Doe")))));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "active")), REFCTMP(new(Boolean, false)))));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "age")), REFCTMP(new(Integer, 45)))));

    JsonObject * json = new(JsonObject, (String *) REFCTMP(new(String, "my_object")), a);
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "List"));
    ASSERT_NOT_NULL(json->value);
    List * value = (List *) json->value;
    ASSERT_EQUAL(value->length, 3);
    ASSERT_STRINGS_EQUAL(call(json->name, to_cstring), "my_object");
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "\"my_object\": {\"name\": \"John Doe\", \"active\": false, \"age\": 45}");
    REFCDEC(json_string);
    REFCDEC(json);
}

void root_object_to_json(TEST_CASE_ARGUMENTS) {
    List * a = REFCTMP(new(List));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "name")), REFCTMP(new(String, "John Doe")))));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "active")), REFCTMP(new(Boolean, false)))));
    call(a, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "age")), REFCTMP(new(Integer, 45)))));

    JsonObject * json = new(JsonObject, a);
    ASSERT_NOT_NULL(json);
    ASSERT_TRUE(type_equal(json->value, "List"));
    ASSERT_NOT_NULL(json->value);
    List * value = (List *) json->value;
    ASSERT_EQUAL(value->length, 3);
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring), "{\"name\": \"John Doe\", \"active\": false, \"age\": 45}");
    REFCDEC(json_string);
    REFCDEC(json);
}

void complex_object_to_json(TEST_CASE_ARGUMENTS) {
    List * john = REFCTMP(new(List));
    call(john, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "name")), REFCTMP(new(String, "John Doe")))));
    call(john, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "active")), REFCTMP(new(Boolean, false)))));
    call(john, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "age")), REFCTMP(new(Integer, 45)))));

    Array * children = REFCTMP(new(Array, 2));
    List * alice = REFCTMP(new(List));
    call(alice, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "name")), REFCTMP(new(String, "Alice")))));
    call(alice, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "age")), REFCTMP(new(Integer, 5)))));

    List * bob = REFCTMP(new(List));
    call(bob, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "name")), REFCTMP(new(String, "Bob")))));
    call(bob, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "age")), REFCTMP(new(Integer, 3)))));

    call(children, set, 0, REFCTMP(new(JsonObject, alice)));
    call(children, set, 1, REFCTMP(new(JsonObject, bob)));
    call(john, add, REFCTMP(new(JsonObject, REFCTMP(new(String, "children")), children)));

    JsonObject * json = new(JsonObject, (Object *) john);
    ASSERT_NOT_NULL(json);
    String * json_string = call(json, to_string);
    ASSERT_STRINGS_EQUAL(call(json_string, to_cstring),
     "{\"name\": \"John Doe\", \"active\": false, \"age\": 45, \"children\": [{\"name\": \"Alice\", \"age\": 5}, {\"name\": \"Bob\", \"age\": 3}]}");
    REFCDEC(json_string);
    REFCDEC(json);
}

TEST_CASES_BEGIN
    TEST_CASE(integer_to_json);
    TEST_CASE(long_to_json);
    TEST_CASE(null_to_json);
    TEST_CASE(double_to_json);
    TEST_CASE(string_to_json);
    TEST_CASE(boolean_to_json);
    TEST_CASE(array_to_json);
    TEST_CASE(object_to_json);
    TEST_CASE(root_object_to_json);
    TEST_CASE(complex_object_to_json);
    TEST_CASE(invalid_name);
TEST_CASES_END

