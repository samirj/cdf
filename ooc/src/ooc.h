#ifndef OOC_H
#define OOC_H

#include "ooc_object.h"
#include "ooc_string.h"
#include "ooc_primitives.h"
#include "ooc_array.h"
#include "ooc_list.h"
#include "ooc_map.h"
#include "ooc_datetime.h"
#include "ooc_singleton.h"


#endif

