include Makefile.inc
CORE_OBJECTS := $(foreach m, $(CORE_MODULES), $(m)/src/*.o)
ALL_OBJECTS := $(foreach m, $(MODULES), $(m)/src/*.o)

all: dist

.PHONY: build
.PHONY: test
.PHONY: examples

build:
	$(info Building the following modules: $(MODULES)) 
	$(foreach m, $(MODULES), make -C $(m); )
	make -C test_framework

test: build
	$(foreach m, $(MODULES), make -C $(m) test && ) echo 'All tests passed!'

valgrind: build
	$(foreach m, $(MODULES), make -C $(m) valgrind && ) echo 'All tests passed'

examples: dist
	make -C examples

clean:
	rm -rf dist
	make -C test_framework clean
	make -C examples clean
	$(foreach m, $(MODULES), make -C $(m) clean; )

dist: build
	mkdir -p dist/lib
	mkdir -p dist/include
	$(foreach m, $(MODULES), cp $(m)/src/*.h dist/include; )
	$(foreach m, $(OPTIONAL_MODULES), cp $(m)/src/*.so dist/lib; )
	$(foreach m, $(MODULES), echo '#include "$(m).h"' >> dist/include/cdf.h; )
	$(CC) $(LDFLAGS) -shared $(CORE_OBJECTS) -o dist/lib/libcdfcore.so
	$(CC) $(LDFLAGS) -shared $(ALL_OBJECTS) -o dist/lib/libcdf.so


