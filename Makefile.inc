
CC=c99
PROJECT_ROOT := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
LIBCDF_ROOT:=$(PROJECT_ROOT)dist
CFLAGS=-c -Wall -fPIC -Os -pthread
LDFLAGS=-pthread

include $(PROJECT_ROOT)Makefile.modules

CORE_MODULES=ooc io
MODULES=$(CORE_MODULES) $(EXTRA_MODULES)
